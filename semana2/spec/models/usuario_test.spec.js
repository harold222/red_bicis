var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario'); 
var Reserva = require('../../models/reserva'); 
var request = require('request'); 
var server = require('../../bin/www');

describe('testing usuarios', function() {
	beforeEach(function(done) { 
		var mongoDB = 'mongodb://localhost/testdb';
		mongoose.connect(mongoDB, {useNewUrlParser:true });

		const db = mongoose.connection;
		db.on('error',console.error.bind(console,'connection error')); // modulo 2
		db.once('open', function(){
			console.log("¡Estamos conectados a la base de datos/USUARIO!");
			done();
		});
	});

	afterEach(function(done) {
		Reserva.deleteMany({}, function(err,success){
			if (err) console.log(err);
			Usuario.deleteMany({}, function(err, success){
				if (err) console.log(err);
				Bicicleta.deleteMany({}, function(err, success){
					if(err) console.log(err);
					done(); //control de asincronismo // eliminar
				});
			});	
		});
	});

	describe('Cuando un usuario reserva una bici', () => {
		it ('debe existir la reserva', (done) => {
			const usuario = new Usuario({nombre: 'Maria'});
			usuario.save();
			const bicicleta = new Bicicleta({code:1, color: "verde", modelo: "urbana", "lat": 41.38189980, "lng": 2.12392250090});
			bicicleta.save();

			var hoy = new Date();
			var mañana = new Date();
			mañana.setDate(hoy.getDate()+1);
			usuario.reservar(bicicleta.id, hoy, mañana, function(err, reverva){
				Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err,reservas){
					console.log(reservas[0]);
					expect(reservas.length).toBe(1);
					expect(reservas[0].diasReserva(1)).toBe(2);
					expect(reservas[0].bicicleta.code).toBe(1);
					expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
					done();
				});
			});

		});
	});

});
