var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req, res){
	res.render('bicicletas/index',{bicis: Bicicleta.allBicis});

}

exports.bicicleta_crear_get = function(req,res){ /*acceso a la pagina de creacion*/
	res.render('bicicletas/crear');

}



exports.bicicleta_crear_post = function(req,res){ /*acceso a la pagina de creacion*/
	var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
	bici.ubicacion = [req.body.lat, req.body.lng];
	Bicicleta.add(bici);

	res.redirect('/bicicletas'); /*vamos a rutas*/

}

exports.bicicleta_update_get = function(req,res){ /*acceso a la pagina de creacion*/
	var bici = Bicicleta.findById(req.params.id);

	res.render('bicicletas/update', {bici});

}


exports.bicicleta_update_post = function(req,res){ /*acceso a la pagina de creacion*/
	var bici = Bicicleta.findById(req.params.id);
	bici.id = req.body.id;
	bici.color = req.body.color;
	bici.modelo = req.body.modelo;
	bici.ubicacion = [req.body.lat, req.body.lng];

	res.redirect('/bicicletas'); /*vamos a rutas*/

}

exports.bicicleta_delete_post = function(req, res){
	Bicicleta.removeById(req.body.id);

	res.redirect('/bicicletas'); /*vamos a rutas*/
}