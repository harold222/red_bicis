const express = require('express');
const router = express.Router();
const indexController = require('../controllers/index');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/login', indexController.login_get);
router.post('/login', indexController.login_post);

router.get('/logout', indexController.logout);

router.get('/forgotPassword', indexController.forgotPassword_get);
router.post('/forgotPassword', indexController.forgotPassword_post);

router.get('/resetPassword/:token', indexController.resetPassword_get);
router.post('/resetPassword', indexController.resetPassword_post);

module.exports = router;