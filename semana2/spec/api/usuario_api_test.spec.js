var mongoose = require('mongoose');
var Usuario = require('../../models/usuario'); 
var Bicicleta = require('../../models/bicicleta');
var Reserva = require('../../models/reserva'); 
var server = require('../../bin/www');
var request = require('request'); 



var base_url = "http://localhost:3000/api/usuarios" // m2

describe('Usuario API', () => {
	beforeEach(function(done) { // conectar
		var mongoDB = 'mongodb://localhost/testdb';
		mongoose.connect(mongoDB, {useNewUrlParser:true});
		const db = mongoose.connection;
		db.on('error', console.error.bind(console,'connection error')); // modulo 2
		db.once('open', function(){
			console.log("¡Estamos conectados a la base de datos/API U!");
			done();
		});
	});

	afterEach(function(done) {
		Reserva.deleteMany({}, function(err,success){
			if (err) console.log(err);
			Usuario.deleteMany({}, function(err, success){
				if (err) console.log(err);
				Bicicleta.deleteMany({}, function(err, success){
					if(err) console.log(err);
					done(); //control de asincronismo // eliminar
				});
			});	
		});
	});

	describe('GET USUARIOS /', () => {
		it("Status 200", (done) => {
			request.get(base_url, function(error, response, body) {
				var result = JSON.parse(body);
				//console.log(response);
				expect(response.statusCode).toBe(200);
				expect(result.usuarios.length).toBe(0);
				done();
			});
		});
	});

	describe('POST USUARIOS /create', () => {
		it("STATUS 200", (done) => { //callback done, hasque done no se ejecuta, no termina el test. sirve para el asincronismo. Asegura obtener el resultado del request. 
			var headers = {'content-type' : 'application/json'};
			var user = '{"nombre": "Maria"}';
			request.post({
				headers: headers, 
				url: base_url + '/create',
				body: user
			  }, function(error, response, body) {
					expect(response.statusCode).toBe(200);
					var usuarios = JSON.parse(body);
					//console.log(body);
					expect(usuarios.nombre).toBe("Maria");
					done();
			});
		});
	});

	describe('API usuarios/reservar', () => {
		it('status 200', (done) => {
			const usuario = new Usuario({nombre: 'Maria'});
			usuario.save();
			const bici = new Bicicleta({code:1, color: "verde", modelo: "urbana", "lat": 41.38189980, "lng": 2.12392250090});
			bici.save();
			console.log(usuario._id, bici._id);
			var reserva = '{"id": "' +  usuario._id + '",  "bici_id": "' + bici._id + '", "desde": "2020-07-17",  "hasta": "2020-07-18"}';
			console.log(reserva);
			request.post({
				headers :  {'content-type' : 'application/json'},
				url: base_url + '/reservar',
				body: reserva
			}, function(error, response, body) {
				expect(response.statusCode).toBe(200);
				//var contenido = JSON.parse(body);
				expect(body.length).toBe(0);
				done();
				
			});

		});
	});
});
