var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta'); 
var server = require('../../bin/www');
var request = require('request'); 



var base_url = "http://localhost:3000/api/bicicletas" // m2

describe('Bicicleta API', () => {
	beforeEach(function(done) { // conectar
		var mongoDB = 'mongodb://localhost/testdb';
		mongoose.connect(mongoDB, {useNewUrlParser:true});

		const db = mongoose.connection;
		db.on('error', console.error.bind(console,'connection error')); // modulo 2
		db.once('open', function(){
			console.log("¡Estamos conectados a la base de datos/API!");
			done();
		});
	});

	afterEach(function(done) {
		Bicicleta.deleteMany({}, function(err, success){
			if(err) console.log(err);
			done(); //control de asincronismo // eliminar
		});
	});

	describe('GET BICICLETAS /', () => {
		it("Status 200", (done) => {
			request.get(base_url, function(error, response, body) {
				var result = JSON.parse(body);
				//console.log(response);
				expect(response.statusCode).toBe(200);
				expect(result.bicicletas.length).toBe(0);
				done();
			});
		});
	});

	describe('POST BICICLETAS /create', () => {
		it("STATUS 200", (done) => { //callback done, hasque done no se ejecuta, no termina el test. sirve para el asincronismo. Asegura obtener el resultado del request. 
			var headers = {'content-type' : 'application/json'};
			var aBici = '{"code":10, "color":"rojo", "modelo":"urbana", "lat":4.5968568, "lng":74.073188}';
			request.post({
				headers: headers, 
				url: base_url + '/create',
				body: aBici
			  }, function(error, response, body) {
					expect(response.statusCode).toBe(200);
					var bici = JSON.parse(body);
					//console.log(body);
					expect(bici.color).toBe("rojo");
					expect(bici.ubicacion[0]).toBe(4.5968568);
					expect(bici.ubicacion[1]).toBe(74.073188);
					done();
			});
		});
	});

	describe('Borrar bicicletas /delete', () => {
		it('status 204', (done) => {
			var a = Bicicleta.createInstance(1, "negro", "urbana", [4.5968568,74.073188]);
			Bicicleta.add(a, function(err){
				//var headers = {'content-type' : 'application/json'};
				var aBici = '{"id": 1}';
				request.delete({
				url: base_url + '/delete',
				body: aBici
				  }, function(error,response,body) {
						expect(response.statusCode).toBe(204);
						//console.log(response);
						expect(body.length).toBe(0);
						done();
					});
			});
		});
	});
});
