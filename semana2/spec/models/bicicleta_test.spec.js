var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('testing Bicicletas', function(){
	beforeEach(function(done) { // conectar
		var mongoDB = 'mongodb://localhost/testdb'
		mongoose.connect(mongoDB, {useNewUrlParser:true});

		const db = mongoose.connection;
		db.on('error',console.error.bind(console,'connection error')); // modulo 2
		db.once('open', function(){
			console.log("¡Estamos conectados a la base de datos!");
			done();
		});
	});

	afterEach(function(done) {
		Bicicleta.deleteMany({}, function(err, success){
			if(err) console.log(err);
			done(); //control de asincronismo // eliminar
		});
	});

	describe('Bicicleta.createInstance', ()=> { // test de confianza :3
		it('Crea una instancia de bicicleta', () => {
			var bici = Bicicleta.createInstance(1, "verde", "Urbana", [4.5968568,-74.073188]);

			expect(bici.code).toBe(1);
			expect(bici.color).toBe("verde");
			expect(bici.modelo).toBe("Urbana");
			expect(bici.ubicacion[0]).toBe(4.5968568);
			expect(bici.ubicacion[1]).toBe(-74.073188);
		})
	});

	describe('Bicicleta.allBicis', () => {
		it('Lista comienza vacia', (done) =>{
			Bicicleta.allBicis(function(err, bicis){
				expect(bicis.length).toBe(0);
				done();
			});
		});
	});

	describe('Bicicleta.add', ()=> {
		it('Agrega una bici', (done)=>{
			var aBici = new Bicicleta({code : 1, color	: "verde",	modelo	: "Urbana"});
			Bicicleta.add(aBici, function(err,newBici){
				if (err) console.log(err);
				Bicicleta.allBicis(function(err, bicis){
					expect(bicis.length).toEqual(1);
					expect(bicis[0].modelo).toEqual(aBici.modelo);

					done();
				});
			});
		});
	});

	describe('Bicicleta.findByCode', () =>{
		it('Debe devolver la bici con id 1', (done) =>{
			Bicicleta.allBicis(function(err,bicis){
				expect(bicis.length).toBe(0);

				var aBici = new Bicicleta({code : 1, color	: "verde",	modelo	: "Urbana"});
				Bicicleta.add(aBici, function(err,newBici){
					if (err) console.log(err);

					var aBici2 = new Bicicleta({code : 2, color	: "Rojo",	modelo	: "Urbana"});
					Bicicleta.add(aBici2, function(err, newBici){
						if(err) console.log(err);
						Bicicleta.findByCode(1, function(error, targetBici){
							expect(targetBici.code).toBe(aBici.code);
							expect(targetBici.color).toBe(aBici.color);
							expect(targetBici.modelo).toBe(aBici.modelo);

							done();
						});

					});
				});
			});
		});
	}); 

});



// lo de abajo se comenta en el modulo 2 y pertenece al modulo 1 sin mongoDB. 

/*beforeEach(() => {Bicicleta.allBicis = []; });
beforeEach(function(){console.log(`testeando…`); }); 


describe("testeando", function() {
  var a;

  it("probando el mensaje", function() {
    a = true;

    expect(a).toBe(true);
  });
});

describe("testeando", function() {
  var a;

  it("probando el mensaje 2", function() {
    a = true;

    expect(a).toBe(true);
  });
});
    



describe('Bicicleta.allBicis', () => { 
	it('comieza vacia', () => {
		expect(Bicicleta.allBicis.length).toBe(0);
	});
}); // grupo de tests

describe('Bicicleta.add', () => {
	it('Agregamos una', () => {
		expect(Bicicleta.allBicis.length).toBe(0);

		var a = new Bicicleta(1, 'Roja', 'Urbana', [4.5968568,-74.073188]);
		Bicicleta.add(a);

		expect(Bicicleta.allBicis.length).toBe(1);
		expect(Bicicleta.allBicis[0]).toBe(a);

	});
});

describe('Bicicleta.findById', () => {
	it('Debe devolver la bici con id 1', () => {
		expect(Bicicleta.allBicis.length).toBe(0);

		var aBici = new Bicicleta(1, 'Caoba', 'Urbana');
		var aBici2 = new Bicicleta(2, 'Lila', 'Rural');
		Bicicleta.add(aBici);
		Bicicleta.add(aBici2);

		var targetBici = Bicicleta.findById(1);
		expect(targetBici.id).toBe(1);
		expect(targetBici.color).toBe(aBici.color);
		expect(targetBici.modelo).toBe(aBici.modelo);


	});
});


/*describe('Bicicleta.removeById', () => {
	it('Debe borrar la bici con id 1', () => {
		expect(Bicicleta.allBicis.length).toBe(0);

		var aBici = new Bicicleta(1, 'Negra', 'Urbana');
		var aBici2 = new Bicicleta(2, 'Azul', 'Rural');
		Bicicleta.add(aBici);
		Bicicleta.add(aBici2);

		var len = Bicicleta.allBicis.length; 
		var targetBici = Bicicleta.removeById(1);
		var len1 = Bicicleta.allBicis.length; 
		expect(len1).toBe(len-1);


	});
});*/
