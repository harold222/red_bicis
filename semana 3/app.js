require('dotenv').config();
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const passport = require('./config/passport');
const session = require('express-session');
const loggedIn = require('./middlewares/loggedIn'); // middleware de verificacion
const validarUsuario = require('./middlewares/validarUsuario'); // middleware JWT

const indexRouter = require('./routes/index');
const usuariosRouter = require('./routes/usuarios');
const tokenRouter = require('./routes/token');
const bicicletasRouter = require('./routes/bicicletas');
const bicicletasAPIRouter = require('./routes/api/bicicletas');
const usuariosAPIRouter = require('./routes/api/usuarios');
const authAPIRouter = require('./routes/api/auth');

const store = new session.MemoryStore; // queda en memoria del servidor -> agil

const app = express();

app.set('secretKey', 'jwt_pwd_!!223344');

app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 1000 },
  store: store,
  saveUninitialized: true,
  resave: true,
  secret: 'red_bicicletas!@#$%^&*()'
}));

const mongoose = require('mongoose');

// si estoy en el ambiente de desarrollo usar localhost:
// const mongoDB = 'mongodb://localhost/red_bicicletas';
// si no, usar: 'mongodb+srv://admin:Gs9ypNlGF35d1cgV@red-bicicletas.cehgw.mongodb.net/red_bicicletas?retryWrites=true&w=majority
const mongoDB = process.env.MONGO_URI;
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error: '));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/privacy_policy', (req, res, next) => { res.render('privacy_policy')});

app.use('/', indexRouter);

app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter);

app.use('/bicicletas', loggedIn, bicicletasRouter);

app.use('/api/auth', authAPIRouter);
app.use('/api/bicicletas', validarUsuario, bicicletasAPIRouter);
app.use('/api/usuarios', usuariosAPIRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;